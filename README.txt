I created this folder structure from referencing the following links:

https://medium.com/swlh/how-to-structure-a-python-based-data-science-project-a-short-tutorial-for-beginners-7e00bff14f56

https://github.com/mishaberrien/standardize-py

https://github.com/mishaberrien/kickstarter_campaign_classification/tree/master/src

.gitlab-ci.yml from this link:

https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml