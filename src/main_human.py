
import sys

sys.path.insert(1, './d00_utils/')
sys.path.insert(2, './d01_data/')
sys.path.insert(3, './d02_agent/')

from memory_data import Experience_Replay
from map_gen import Map_Gen
from human import Human

def main():
    #existing, test_experience.csv, test_experience.csv, 5, 30, 3
    
    map_type_name = input("Input Map Type (random/existing): ")    
    map_file_name = input("Input map file name. Skip if selected random. (add .csv to end): ")
    memory_file_name = input("Input memory file name (add .csv to end): " )
    
    
    map_width = int(input("Input map width: "))
    map_height = int(input("Input map height: "))
    map_vision = int(input("Input map vision: "))
    game_map = Map_Gen(map_type_name, map_width, map_height, map_vision, map_file_name)
    
    if (not game_map.get_found_map()):
        input("Map Not Found. Exiting System. Please do a better job at spelling next time.")
        sys.exit()
        
    game_map.print_map_info()
    map_name, map_type, game_map, width, height, vision = game_map.get_map_info()
    # check to see if map name exists if it exists then ask user if he wants map or he wants to override data
    # make sure hard to override
    memory = Experience_Replay(map_name,vision, width, memory_file_name)
    #memory.print_experience_replay_array()

   
    agent = Human(map_name, game_map, width, height, vision, memory)
    agent.print_turn_message()

    while(agent.get_is_game_ongoing()):
        player_action = input()
        agent.player_input(player_action)
    print("Experience Gathered: ", memory.get_experience_replay_array())
    print("Total Reward accumulated: " + str(agent.get_total_reward()))
    
    is_memory_choice_selected = False
    while (not is_memory_choice_selected):
        preserve_option = input("Do you want to preserve memory? (y/n)")
        if (preserve_option == 'y'):
            is_memory_choice_selected = True
            #call memory data here
            print("Preserving memory. Calling save memory function.")
            memory.save_configuration()
        elif(preserve_option == 'n'):
            is_memory_choice_selected = True
            print("Memory not preserved. Exiting the game...")
    
    

if __name__ == "__main__":
    main()

