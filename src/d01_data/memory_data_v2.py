# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 19:28:04 2020

@author: Dingie
"""

from os import path
import csv
class Memory_Data_V2:
    
    INIT_PATH = "../data/02_data_memory/"
    memory_file_name = ""
    experience_replay_array = []
    
    width = None
    height = None
    vision = None
    
    memory_data_limit = 1000
    
    def __init__(self):
        print("Creating Memory Data object...")
    
    def configure(self, width, height, vision):
        self.vision = vision
        self.width = width
        self.height = height
        self.memory_file_name = input("Input memory file name (add .csv to end): " )
    
    def check_configure(self):
        if (self.check_memory_data_exist()):
            if(self.check_memory_data_parameter()):
                print("Memory data parameter confirmed correct.")
                self.set_memory_data()
                print("Memory data configured successfully.")
                return None
        print("Memory data not configured.")
        print("Memory check completed.")
        return None

    def check_memory_data_exist(self):
        if (path.exists(self.INIT_PATH + self.memory_file_name) and self.memory_file_name != ""):
            print("Memory data confirmed to exist.")
            return True
        print("Memory data not found.")
        return False
    
    def check_memory_data_parameter(self):
        self.fetch_memory_data()
        self.print_experience_replay_array()

        if (len(self.experience_replay_array[0][0]) == self.width):
            print("Experience file width same as map width.")
        else:
            print("Experience file width not the same as map width. Please check data for corruption.")
            print("Will not use previously saved experience.")
            self.experience_replay_array = []
            return False
        
        if (len(self.experience_replay_array[0][1]) == self.vision):
            print("Experience file vision same as map vision.")
        else: 
            print("Experience file vision not the same as map vision. Please check data for corruption.")
            print("Will not use previously saved experience.")
            self.experience_replay_array = []
            return False
        return True
        
    def set_memory_data(self, new_experience_replay_array):
        self.experience_replay_array = new_experience_replay_array
    
    def get_memory_data(self):
        return self.experience_replay_array
    
    def print_experience_replay_array(self):
        print(self.experience_replay_array)
    
    def fetch_memory_data(self):
         with open(self.INIT_PATH+self.memory_file_name) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=",")
            state_vision_counter = 0
            temp_experience_array = []
            temp_state_array = []
            for row in readCSV:
                #print(row)
                #print(row[0])
                if (row[0] == 'B'):
                    print("Starting experience retrieval...")
                #if (row[0] == 'I'):
                #    temp_experience_array = []
                #    print("Located experience ID: " + row[1])
                #    temp_experience_array.append(float(row[1]))
                #    print("Added experience ID: " + row[1] + " to temp experience array")
                if (row[0] == 'CP'):
                    print("Located current position: " + row[1])
                    temp_experience_array.append(list(map(float,(row[1:]))))
                    print("Added current position to temp experience array")
                if (row[0] == 'CS'):
                    print("Located current state...")
                    state_vision_counter+= 1
                    temp_state_array.append(list(map(float,(row[1:]))))
                    print("Added current state to temp state array")
                    if (state_vision_counter == self.vision):
                        temp_experience_array.append(temp_state_array)
                        temp_state_array = []
                        state_vision_counter = 0
                        print("Added current state to temp experience array")

                if (row[0] == 'A'):
                    print("Located action state...")
                    temp_experience_array.append(float(row[1]))
                    print("Added action state to temp experience array")
                if(row[0] == 'R'):
                    print("Located reward state...")
                    temp_experience_array.append(float(row[1]))
                    print("Added reward state to temp experience array")
                if (row[0] == 'NS'):
                    print("Located next state...")
                    state_vision_counter+= 1
                    temp_state_array.append(list(map(float,(row[1:]))))
                    print("Added next state to temp state array")
                    if (state_vision_counter == self.vision):
                        temp_experience_array.append(temp_state_array)
                        temp_state_array = []
                        state_vision_counter = 0
                        self.experience_replay_array.append(temp_experience_array)
                        temp_experience_array = []
                        print("Added next state to temp experience array")
                
            print("Finished adding existing experience replay to experience_replay_array")