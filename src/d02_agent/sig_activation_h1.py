# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 17:56:28 2020

@author: Dingie
"""

import numpy as np

class Sig_Activation_H1:
    
    def __init__ (self, input_len, nodes):
        self.target_weights = np.random.randn(input_len, nodes)/ input_len
        self.policy_weights = np.random.randn(input_len, nodes)/ input_len
        self.biases = np.zeros(nodes)
    
    def forward_prop_action(self, input_value):
        input_value = input_value.flatten()
        totals = np.dot(input_value, self.policy_weights) + self.biases
        return self.sigmoid(totals)
    
    def forward_prop_target(self, input_value):
#        print("inside forward_prop")
#        print(input_value)
        self.last_input_shape = input_value.shape
#        print("input value shape")
#        print(input_value.shape)
        self.last_input_value = input_value
        input_len, nodes = self.target_weights.shape
        totals = np.dot(input_value, self.target_weights) + self.biases
#        print("relu linear calculation result")
#        print(totals)
        self.last_totals = totals
#        print("relu activation calculation result")
#        print(self.relu_activation_equation(totals))
        return self.sigmoid(totals)  
    
    def forward_prop(self, input_value):
#        print("inside forward_prop")
#        print(input_value)
        self.last_input_shape = input_value.shape
#        print("input value shape")
#        print(input_value.shape)
        self.last_input_value = input_value
        input_len, nodes = self.policy_weights.shape
        totals = np.dot(input_value, self.policy_weights) + self.biases
#        print("relu linear calculation result")
#        print(totals)
        self.last_totals = totals
#        print("relu activation calculation result")
#        print(self.relu_activation_equation(totals))
        return self.sigmoid(totals)
    
    def backward_prop(self, d_net_d_out, error_map, d_net_d_out_hidden, d_out_d_net_hidden, learn_rate= 0.00001):
        
#        print("wz, mse, wy, relu")
#        print(d_net_d_out)
#        print(d_net_d_out.shape)
#        print(error_map)
#        print(error_map.shape)
#        print(d_net_d_out_hidden)
#        print(d_net_d_out_hidden.shape)
#        print(d_out_d_net_hidden)
#        print(d_out_d_net_hidden.shape)
#        
#        print(self.last_input_value)
#        print(self.last_input_value.shape)
#        print(self.last_totals)
#        print(self.last_totals.shape)   
        d_error_d_out = np.dot(error_map, np.transpose(d_net_d_out))
        d_error_d_net_2 = d_error_d_out * d_out_d_net_hidden
        d_error_d_out_1 = np.dot(d_error_d_net_2 ,np.transpose(d_net_d_out_hidden))
#        d_out_d_out_hidden = np.dot(d_net_d_out_hidden, np.transpose(d_out_d_net_hidden))
        
#        d_error_d_out_hidden = d_error_d_out * d_out_d_out_hidden
        
        
        d_out_hidden_d_net_input = self.sigmoid_derivative(self.last_totals)
        d_error_d_net_input = d_error_d_out_1 * d_out_hidden_d_net_input
        d_error_d_w_input = np.dot(np.transpose(self.last_input_value), d_error_d_net_input)
        
#        d_out_d_w_input = self.last_input_value * d_out_hidden_d_net_input
    
#        d_error_d_net_hidden= d_out_hidden_d_net_input * np.transpose(d_error_d_out_hidden)
    
#        d_error_d_w_input = np.dot(np.transpose(self.last_input_value), d_error_d_net_hidden)
#        print(d_error_d_w_input)
        self.policy_weights = self.policy_weights - learn_rate * d_error_d_w_input
        return None
    
    def set_target_weights(self, new_weights):
        self.target_weights = new_weights
    
    def get_target_weights(self):
        return self.target_weights
    
    def get_policy_weights(self):
        return self.policy_weights
    
    def get_biases(self):
        return self.biases
    
    def sigmoid(self, linearRegression):
        return 1/(1+np.exp(-linearRegression))

    def sigmoid_derivative(self, sigmoidOutput):
        return self.sigmoid(sigmoidOutput)*(1-self.sigmoid(sigmoidOutput))

    
    def save_weights(self):
        return None
            
    def print_target_weights(self):
        print("Target Weights: \n")
        print(self.target_weights)
        
    def print_policy_weights(self):
        print("Policy Weights: \n")
        print(self.policy_weights)

    def print_biases(self):
        print(self.biases)