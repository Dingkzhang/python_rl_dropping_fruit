# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 23:40:17 2020

@author: Dingie
"""

import random
from map_gen_v2 import Map_Gen_V2
from memory_data_v2 import Memory_Data_V2
from neural_network_v2 import Neural_Network_V2
from cost_function import Cost_Function

import numpy as np

class Robot:
    
    backpropagation_batch_size = 200
    
    #start weight adjustment when len of array is 20
    #when weight adjustment init is reached weight_counter and decision_selection_counter starts
    weight_adjustment_init = 1000
    weight_adjustment_boolean = False
    
    #after x increments update the target weight with the policy weight
    weight_update_counter = 0
    weight_update_limit = 10000
    divisible_constant = 500

    #after x increments decrement the boundary value between exploration and exploitation. Ensures that the robot will be exploiting more in future
    decision_selection_counter = 0
    decision_selection_update_limit = 1000
    decision_selection_boundary = 1
    decision_selection_decrement_value = 0.1
    
    game_map_object = None
    memory_data_object = None
    neural_network_object = None
        
    game_map = None
    width = None
    height = None 
    vision = None

    experience_replay_array = []
    experience_replay_max_capacity = 10000
    hidden_layer_one = None
    
    hidden_layer_two = None
    output_layer = None
    softmax_layer = None
    
    current_position = None
    current_position_array = []
    next_position = None
    next_position_array = []
    
    current_field_array = []
    next_field_array = []
    
    current_input_field_array = []
    next_input_field_array = []
    
    flattened_network_array = []
    
    capture_current_position_array = None
    capture_action = None
    capture_current_field = None
    capture_next_field = None
    capture_reward = None
    
    total_reward = 0
    total_reward_iter = 0
    total_reward_iter_holder = 0
    
    current_row = 0
    current_error = 0
    num_decisions = 3
    num_iter = 0
    
    error_result = 0
    
    def __init__(self):
        return None
    
    def configure_game_map(self, game_map):
        self.game_map_object = game_map
        self.game_map = self.game_map_object.get_map()
        game_map_info_array = self.game_map_object.get_map_info()
        self.width = game_map_info_array[0]
        self.height = game_map_info_array[1]
        self.vision = game_map_info_array[2]
        
    def configure_memory_data(self, memory_data):
        self.memory_data_object = memory_data
        self.experience_replay_array =  self.memory_data_object.get_memory_data()
    
    def configure_neural_network(self, neural_network):
        self.neural_network_object = neural_network
        self.hidden_layer_one = self.neural_network_object.get_hidden_layer_one()
        self.hidden_layer_two = self.neural_network_object.get_hidden_layer_two()
        self.output_layer = self.neural_network_object.get_output_layer()
        self.softmax_layer = self.neural_network_object.get_softmax_layer()
    
    def configure_robot_simulation(self):
        self.configure_start_field()
        self.configure_start_position()
    
    def configure_start_field(self):
        for i in range(self.vision):
            self.current_field_array.append(self.game_map[i])
        for i in range(self.vision):
            self.current_input_field_array.append(self.game_map[i+1])
        self.current_input_field_array = self.current_input_field_array
        self.current_row += 1
        print("Current field.")
        print(self.current_field_array)
        print("Current input field.")
        print(self.current_input_field_array)
        
    def configure_start_position(self):
        print("Creating robot start position.")
        self.current_position = self.width //2 
        self.next_position = self.current_position
        current_position_array = []
        for i in range(self.width):
            if (self.current_position == i):
                current_position_array.append(1)
            else:
                current_position_array.append(0)
        self.current_position_array = current_position_array
        
        print(self.current_position_array)
    
    def select_explore_exploit(self):
#        print("Choosing to either explore or exploit")   
        action_decision_selection = random.uniform(0,1)
#        print("Decision value: " + str(action_decision_selection))
        if (action_decision_selection > self.decision_selection_boundary):
            return "exploit"
        return "explore"
    
    def execute_action(self, action):
        if (action == "explore"):
#            print("Robot chooses to explore")
#            print("Generating random action")
            move_selected = random.randint(-1,1)
            return move_selected
        if (action == "exploit"):
#            print("Robot chooses to exploit")
#            print("Executing forward propagation")
            move_array = self.execute_nn_decision()
            move_index = np.argmax(move_array)
            if (move_index == 0):
                move_selected = -1
            elif(move_index == 1):
                move_selected = 0
            else:
                move_selected = 1
                
            return move_selected
            
    def get_current_state(self):
        return None
    
    def execute_nn_decision(self):
        flattened_input_field_array = np.array(self.current_input_field_array).flatten()
        flattened_current_position_array = np.array(self.current_position_array).flatten()
        self.flattened_network_array = np.concatenate((flattened_input_field_array, flattened_current_position_array), axis=None)
#        print(self.flattened_network_array.shape)
#        self.flattened_network_array = self.create_nn_decision_feature(self.flattened_network_array)
        hidden_layer_one_result = self.hidden_layer_one.forward_prop_action(self.flattened_network_array)
        hidden_layer_two_result = self.hidden_layer_two.forward_prop_action(hidden_layer_one_result)
        move_array = self.output_layer.forward_prop_action(hidden_layer_two_result)
#        move_array = self.softmax_layer.softmax_activation_equation(move_array)
#        print("Move array")
#        print(move_array)
        return move_array
    
    def create_nn_decision_feature(self, feature_array):
#        print("edkere")
#        print(feature_array)
        current_array = np.reshape(feature_array, (self.vision+1, self.width))
        current_array = np.delete(current_array, self.width, 0)
        column_feature = np.sum(current_array, axis=0)
        row_feature = np.sum(current_array, axis=1)
#            print(current_array)
#        print(column_feature)
        feature_array = np.concatenate((feature_array, column_feature), axis=None)
        feature_array = np.concatenate((feature_array, row_feature), axis=None)

#        print(feature_array)
        return feature_array
    
    # capture experience = configure states new states, rewards, save to memory_data etc...
    def capture_experience(self):
        return None
    
    def configure_new_state(self, move_selected):
        self.configure_position(move_selected)
        self.configure_field()
        self.configure_reward()
        
    def configure_position(self, input_action):
        if (self.current_position + input_action >= self.width -1):
            self.next_position = self.width - 1
        elif(self.current_position + input_action <=0):
            self.next_position = 0
        else:
            self.next_position = self.current_position + input_action
        
        self.next_position_array = np.zeros(self.width)
        self.next_position_array[self.next_position] = 1
        self.capture_current_position_array = self.current_position_array
        self.capture_action = input_action
        self.current_position_array = self.next_position_array
        self.current_position = self.next_position
        
    def configure_field(self):
        self.capture_current_field = []
        self.next_input_field_array = []
        self.capture_current_field = self.current_input_field_array
        self.current_row += 1
        self.next_field = []
        if (self.current_row + self.vision <= self.height):
            for i in range(self.current_row, self.current_row + self.vision):
                self.next_input_field_array.append(self.game_map[i])
        elif(self.current_row + self.vision > self.height):
            zero_row_needed = self.current_row + self.vision - self.height
            for i in range(self.current_row, self.height):
                self.next_input_field_array.append(self.game_map[i])
                
            for i in range(zero_row_needed):
                self.next_input_field_array.append(np.zeros(self.width))
        self.capture_next_field = self.next_input_field_array
        self.current_input_field_array = self.next_input_field_array
        
    def configure_reward(self):
        self.capture_reward = str(self.capture_current_field[0][self.next_position])
        self.total_reward = self.total_reward + float(self.capture_reward)
        self.total_reward_iter = self.total_reward_iter + float(self.capture_reward)
        if (self.num_iter % self.divisible_constant == 0):
            self.total_reward_iter_holder = self.total_reward_iter
            self.total_reward_iter = 0
    
    def is_experiment_on_going(self):
        if (self.height == self.current_row):
            return False
        return True
    
    def store_experience(self):
        
        experience_array = []
        experience_array.append(self.capture_current_position_array)
        experience_array.append(self.capture_current_field)
        experience_array.append(self.capture_action)
        experience_array.append(float(self.capture_reward))
        experience_array.append(self.capture_next_field)
        self.experience_replay_array.append(experience_array)
    
    def check_store_experience_capacity(self):
        if (self.experience_replay_max_capacity <= len(self.experience_replay_array)):
            return True
        return False
    
    def remove_first_experience(self):
        self.experience_replay_array.pop(0)
    
    # if this is true allow for weight adjustment
    # in turn this allows for the weight update counter to start
    # in turn this allows for the increment decision selection counter to start
    def check_store_experience_len(self):
        if (len(self.experience_replay_array) >= self.weight_adjustment_init):
            self.weight_adjustment_boolean = True
            return True
        self.weight_adjustment_boolean = False
        return False
    
    def increment_weight_update_counter(self):
        self.weight_update_counter += 1
    
    def check_weight_update_counter(self):
        if (self.weight_update_counter == self.weight_update_limit):
            self.weight_update_counter = 0
            return True
        return False
    
    #updates the target weight with the policy weight
    def implement_weight_update(self):
#        print("updating target weight with policy weight")
        
        layer_one_policy_weights = self.hidden_layer_one.get_policy_weights()
        self.hidden_layer_one.set_target_weights(layer_one_policy_weights)
        
        layer_two_policy_weights = self.hidden_layer_two.get_policy_weights()
        self.hidden_layer_two.set_target_weights(layer_two_policy_weights)
        
        layer_output_policy_weights = self.output_layer.get_policy_weights()
        self.output_layer.set_target_weights(layer_output_policy_weights)
    
    def increment_decision_selection_counter(self):
        self.decision_selection_counter += 1
        
    def check_decision_selection_counter(self):
        if (self.decision_selection_counter == self.decision_selection_update_limit):
            self.decision_selection_counter = 0
            return True
        return False
        
    def implement_decision_selection(self):
#        print("updating decision selection boundary")
        if (self.decision_selection_boundary > 0.1):
            self.decision_selection_boundary = self.decision_selection_boundary - self.decision_selection_decrement_value
        else:
            self.decision_selection_boundary = self.decision_selection_boundary
#        print("new decision selection boundary: " + str(self.decision_selection_boundary))
    # starts the backpropagation process with a random selection of batches
    def update_weights(self):
#        print("sampling batch for forward propagation")
        sample_batch = self.fetch_experience_replay_batch()
#        print(sample_batch)
        current_state_batch, next_state_batch, reward_batch, action_batch =  self.flatten_batch(sample_batch)
#        current_state_batch, next_state_batch = self.create_new_batch_features(current_state_batch, next_state_batch)
        current_forward_prop_result = self.forward_propagation(current_state_batch)
        next_forward_prop_result = self.forward_propagation_target_weights(next_state_batch)
#        print("Forward prop result")
#        print(current_forward_prop_result)
#        print(next_forward_prop_result)
        error_result, dE_dOut = self.calculate_cost(current_forward_prop_result, next_forward_prop_result, action_batch, reward_batch)
        error_map = self.generate_error_map(action_batch, dE_dOut)
        back_propagation_result = self.back_propagation(error_map)
        
        self.error_result = error_result
        
    def fetch_experience_replay_batch(self):
#        print("fetching replay batch")
        return random.sample(self.experience_replay_array, self.backpropagation_batch_size)

    def flatten_batch(self, sample_batch):
        
        current_state_batch = []
        next_state_batch = []
        reward_batch = []
        action_batch = []
        for x in sample_batch:
            flattened_current_state_array = np.array(x[1]).flatten()
            flattened_current_position_array = np.array(x[0]).flatten()
            current_state_batch.append(np.concatenate((flattened_current_state_array, flattened_current_position_array), axis=None))
            
            flattened_next_state_array = np.array(x[4]).flatten()
            flattened_next_position_array = self.next_position_calculator(x[0], x[2])
            next_state_batch.append(np.concatenate((flattened_next_state_array, flattened_next_position_array), axis= None))
            
            reward_batch.append(x[3])
            
            action_batch.append(x[2])
            
#        print("Current state batch, next state batch, reward_Btach")
#        print(current_state_batch, next_state_batch, reward_batch, action_batch)
        return current_state_batch, next_state_batch, reward_batch, action_batch
    
    def create_new_batch_features(self, current_state_batch, next_state_batch):
#        print(current_state_batch)
#        print(current_state_batch)        
        for x in range(len(current_state_batch)):
            current_array = np.reshape(current_state_batch[x], (self.vision+1, self.width))
            current_array = np.delete(current_array, self.width, 0)
            column_feature = np.sum(current_array, axis=0)
#            print(current_array)
#            print(column_feature)
            row_feature = np.sum(current_array, axis=1)
            current_state_batch[x] = np.concatenate((current_state_batch[x], column_feature), axis=None)
            current_state_batch[x] = np.concatenate((current_state_batch[x], row_feature), axis=None)

#            print(current_state_batch[x])
            
            next_array = np.reshape(next_state_batch[x], (self.vision+1, self.width))
            next_array = np.delete(next_array, self.width, 0)
            column_feature = np.sum(next_array, axis=0)
            row_feature = np.sum(next_array, axis=1)
            next_state_batch[x] = np.concatenate((next_state_batch[x], column_feature), axis=None)
            next_state_batch[x] = np.concatenate((next_state_batch[x], row_feature), axis=None)


        return current_state_batch, next_state_batch
    
    #converts the position moved into an array
    def next_position_calculator(self, current_position, action_selected):
        current_index = np.where(current_position == 1)
        current_index = current_index[0]
        new_index = current_index + action_selected
        
        if (new_index <= 0):
            new_index = 0    
        elif(new_index >= self.width-1):
            new_index = self.width - 1
        
        next_position = np.zeros(self.width)
        next_position[new_index] = 1
        return next_position

    def generate_error_map(self,action_batch, dE_dOut):
#        print("action_batch")
#        print(len(action_batch))
        
        error_batch_array = np.zeros((len(action_batch), self.num_decisions))
        
        for i in range(len(action_batch)):
            if (action_batch[i] == -1):
                error_batch_array[i][0] = dE_dOut
            elif(action_batch[i] == 0):
                error_batch_array[i][1] = dE_dOut
            elif(action_batch[i] == 1):
                error_batch_array[i][2] = dE_dOut
                
#        print(error_batch_array)
        
        return error_batch_array

    # forward propagation need to do the next step of multiplying the thingies
    def forward_propagation(self, flattened_batch):
        flattened_batch_array = np.asarray(flattened_batch)

#        print("forward propagation flattened batch")
#        print(flattened_batch_array)
#        print(flattened_batch_array.shape)
        forward_prop_result = self.hidden_layer_one.forward_prop(flattened_batch_array)
        forward_prop_result = self.hidden_layer_two.forward_prop(forward_prop_result)
        forward_prop_result = self.output_layer.forward_prop(forward_prop_result)
        return forward_prop_result

    def forward_propagation_target_weights(self, flattened_batch):
        
        flattened_batch_array = np.asarray(flattened_batch)
        forward_prop_result = self.hidden_layer_one.forward_prop_target(flattened_batch_array)
        forward_prop_result = self.hidden_layer_two.forward_prop_target(forward_prop_result)
        forward_prop_result = self.output_layer.forward_prop_target(forward_prop_result)
        return forward_prop_result

    def calculate_cost(self, current_prop, next_prop, action, reward):
        cost_function = Cost_Function()
        
        current_prop_value = cost_function.determine_current_prop(current_prop, action)
        next_prop_value = cost_function.determine_next_prop(next_prop)
        calculated_loss = cost_function.calculate_loss(current_prop_value, next_prop_value, reward)
        calculated_error = cost_function.mean_squared_error(calculated_loss)
        calculated_error_derivative = cost_function.mean_squared_error_derivative(calculated_loss)
#        print("calculated error")
#        print(calculated_error)
        self.current_error = calculated_error
        return calculated_error, calculated_error_derivative
        
    def back_propagation(self, error_map):
#        print("Initiating back-propagation")
#        print("Error Derivative")
#        print(error_map)
        d_net_d_out, error_map = self.output_layer.backward_prop(error_map)
        d_net_d_out_hidden, d_out_d_net_hidden = self.hidden_layer_two.backward_prop(d_net_d_out, error_map)
        self.hidden_layer_one.backward_prop(d_net_d_out, error_map, d_net_d_out_hidden, d_out_d_net_hidden)
        return d_net_d_out

    def print_experience_replay_array(self):
        print("Experience replay array")
        print(self.experience_replay_array)
        print("Total experience in array: " + str(len(self.experience_replay_array)))
    
    def print_error(self):
        print(self.current_error)
    
    def print_state_capture(self):
        print("current row")
        print(self.current_row)
        print("print current position array")
        print(self.capture_current_position_array)
        print("print current action")
        print(self.capture_action)
        print("print current field")
        print(self.capture_current_field)
        print("print next field")
        print(self.capture_next_field)
        print("capture reward")
        print(self.capture_reward)
        print("total reward")
        print(self.total_reward)
    
    def increment_iter(self):
        self.num_iter += 1
    
    def print_iter_result(self):        
        if (self.num_iter % self.divisible_constant == 0):
            print("Error Result: "+ str(self.num_iter) + " " + str(self.error_result) + " Total Reward: " + str(self.total_reward) + " Iteration Reward: " + str(self.total_reward_iter_holder))
#            print("Weight Result")
#            print(back_propagation_result)
        return None