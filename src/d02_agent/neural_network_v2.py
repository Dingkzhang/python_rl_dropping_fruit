# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 15:42:11 2020

@author: Dingie
"""

from tanh_activation import Tanh_Activation
from softmax_activation import Softmax_Activation
from leaky_relu_activation_h1 import Leaky_Relu_Activation_H1
from leaky_relu_activation_h2 import Leaky_Relu_Activation_H2
from linear_activation_output import Linear_Activation_Output
from sig_activation_h1 import Sig_Activation_H1
from sig_activation_h2 import Sig_Activation_H2

from tanh_activation_h1 import Tanh_Activation_H1
from tanh_activation_h2 import Tanh_Activation_H2

import numpy as np

from datetime import date

class Neural_Network_V2:
    
    INIT_PATH = "../data/03_data_nn/"
    neural_network_file_name = None
    
    width = None
    height = None
    vision = None
    
    hidden_one_name = None
    hidden_two_name = None
    output_name = None
    
    hidden_layer_one = None
    hidden_layer_two = None
    output_layer = None
    softmax_layer = None
    
    input_size = None
    node_size_one = None
    node_size_two = None
    node_size_three = None
    
    def __init__ (self):
        return None
    
    def configure(self, width, height, vision, hidden_one_name, hidden_two_name, output_name, feature_add_on = 0):
        self.hidden_one_name = hidden_one_name
        self.hidden_two_name = hidden_two_name
        self.output_name = output_name
        self.width = width
        self.height = height
        self.vision = vision
        self.configure_weight_size(feature_add_on)

    def configure_weight_size(self, feature_add_on = 0):
        self.input_size = self.width * self.vision + self.width
        self.node_size_one =  10 * self.input_size // 2
        self.node_size_two =  10 * self.input_size // 2
        self.node_size_three = 3
        self.print_neural_network_info()        

    # most baisc neural network generation, will add additional features in the future
    def generate_neural_network(self):
        print("Generating neural network.")
        if (self.hidden_one_name == "tanh"):
            print("Generating hidden layer one tanh activation.")
            self.hidden_layer_one = Tanh_Activation_H1(self.input_size,self.node_size_one)
        if (self.hidden_one_name == "relu"):
            print("Generating hidden layer one relu activation.")
            self.hidden_layer_one = Leaky_Relu_Activation_H1(self.input_size, self.node_size_one)
        if (self.hidden_one_name == "sig"):
            print("Generating hidden layer one sig activation.")
            self.hidden_layer_one = Sig_Activation_H1(self.input_size, self.node_size_one)
        
        
        if (self.hidden_two_name == "tanh"):
            print("Generating hidden layer two tanh activation.")
            self.hidden_layer_two = Tanh_Activation_H2(self.node_size_one, self.node_size_two)
            
        if (self.hidden_two_name == "relu"):
            print("Generating hidden layer two relu activation.")
            self.hidden_layer_two = Leaky_Relu_Activation_H2(self.node_size_one, self.node_size_two)
        if (self.hidden_two_name =="sig"):
            print("Generating hidden layer two sig activation.")
            self.hidden_layer_two = Sig_Activation_H2(self.node_size_one, self.node_size_two)
        
        if (self.output_name == "softmax"):
            print("Generating output layer softmax activation.")    
            self.output_layer = Softmax_Activation(self.node_size_two, self.node_size_three)
        if (self.output_name == "linear"):
            print("Generating output layer linear activation.")
            self.output_layer = Linear_Activation_Output(self.node_size_two, self.node_size_three)
            
        self.softmax_layer = Softmax_Activation(self.node_size_two, self.node_size_three)
        return None
    
    def get_hidden_layer_one(self):
        return self.hidden_layer_one
    
    def get_hidden_layer_two(self):
        return self.hidden_layer_two
    
    def get_output_layer(self):
        return self.output_layer
    
    def get_softmax_layer(self):
        return self.softmax_layer
    
    def save_hidden_layer_one(self, current_time):
        h1_target_weights = self.hidden_layer_one.get_target_weights()
        h1_policy_weights = self.hidden_layer_one.get_policy_weights()
        np.savetxt(self.INIT_PATH + "h1_target"+str(current_time) + ".csv", h1_target_weights, delimiter=",")
        np.savetxt(self.INIT_PATH + "h1_policy"+str(current_time) + ".csv", h1_policy_weights, delimiter=",")
    
    def save_hidden_layer_two(self, current_time):
        h2_target_weights = self.hidden_layer_two.get_target_weights()
        h2_policy_weights = self.hidden_layer_two.get_policy_weights()
        np.savetxt(self.INIT_PATH + "h2_target"+str(current_time) + ".csv", h2_target_weights, delimiter=",")
        np.savetxt(self.INIT_PATH + "h2_policy"+str(current_time) + ".csv", h2_policy_weights, delimiter=",")
        
    def save_output_layer(self, current_time):
        output_target_weights = self.output_layer.get_target_weights()
        output_policy_weights = self.output_layer.get_policy_weights()
        np.savetxt(self.INIT_PATH + "output_target"+str(current_time) + ".csv", output_target_weights, delimiter=",")
        np.savetxt(self.INIT_PATH + "output_policy"+str(current_time) + ".csv", output_policy_weights, delimiter=",")    
        
    def print_neural_network_info(self):
        format_string = "{:<20s}{:>20s}"    
        print(format_string.format("Input Layer Size: ", str(self.input_size)))
        print(format_string.format("Hidden Layer One Size: ", str(self.node_size_one)))
        print(format_string.format("Hidden Layer Two Size: ", str(self.node_size_two)))
    
    def print_hidden_one_info(self):
        print(self.hidden_one_name + " activation layer (Hidden layer one)\n")
        print(self.hidden_layer_one.print_target_weights())
        print(self.hidden_layer_one.print_policy_weights())
        print(self.hidden_layer_one.print_biases())
        
    def print_hidden_two_info(self):
        print(self.hidden_two_name + " activation layer (Hidden layer two)\n")
        print(self.hidden_layer_two.print_target_weights())
        print(self.hidden_layer_two.print_policy_weights())
        print(self.hidden_layer_two.print_biases())
        
    def print_output_info(self):
        print(self.output_name + " activation layer (Output layer)\n")
        print(self.output_layer.print_target_weights())
        print(self.output_layer.print_policy_weights())
        print(self.output_layer.print_biases())
        
        