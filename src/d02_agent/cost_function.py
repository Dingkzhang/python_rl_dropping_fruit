# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 22:11:58 2020

@author: Dingie
"""

import numpy as np

class Cost_Function:
    
    discount_factor = 0.9
    
    def __init__(self):
        return None
    
    def determine_current_prop(self, current_prop, action):
        #print("current_prop value")
        #print(current_prop)
        #print("action value")
        #print(action)
        
        current_prop_value = []
        
        for i in range(len(action)):
            if (action[i] == -1):
                current_prop_value.append(current_prop[i][0])
            elif(action[i] == 0):
                current_prop_value.append(current_prop[i][1])
            elif(action[i] == 1):
                current_prop_value.append(current_prop[i][2])
                
        #print("actual current prop value")
        #print(current_prop_value)
        return current_prop_value
    
    def determine_next_prop(self, next_prop):
        #print("next_prop value")
        #print(next_prop)
        
        next_prop_value = []
        for data in next_prop:
            next_prop_value.append(data[np.argmax(data)])
            
        #print("actual next prop value")
        #print(next_prop_value)
        return next_prop_value
    
    # reward + discount-factor*maxq - q
    def calculate_loss(self, current_prop_value, next_prop_value, reward):
        
        current_prop_value = np.asarray(current_prop_value)
        next_prop_value = np.asarray(next_prop_value)
        
#        print("prop values")
#        print(current_prop_value)
#        print(next_prop_value)
        optimal_q = np.add(reward, self.discount_factor * next_prop_value)
        target_q = current_prop_value
        
#        print("list of things")
#        print(reward)
#        print(next_prop_value)
#        print(optimal_q)
#        print(target_q)
        return np.subtract(optimal_q, target_q)
    
    
    def cross_entropy_error(self, loss):
        return None
    
    def cross_entropy_error_derivative(self, loss):
        return None
    
    def mean_squared_error(self, loss):
        return np.sum(loss**2)/ (len(loss) * 2)
    
    def mean_squared_error_derivative(self, loss):
        return -1 * np.sum(loss) / (len(loss))