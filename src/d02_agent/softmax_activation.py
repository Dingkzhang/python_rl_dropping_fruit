# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 17:56:20 2020

@author: Dingie
"""

import numpy as np

class Softmax_Activation:
    
    def __init__(self, input_len, nodes):
        self.target_weights = np.random.randn(input_len, nodes)/ input_len
        self.policy_weights = self.target_weights
        self.biases = np.zeros(nodes)
    
    def forward_prop_action(self, input_value):
        input_value = input_value.flatten()
        totals = np.dot(input_value, self.policy_weights) + self.biases
        print("policy weights")
#        print(self.policy_weights)
        print(self.softmax_activation_equation(totals))
        return self.softmax_activation_equation(totals)
    
    def forward_prop(self, input_value):
        self.last_input_shape = input_value.shape
        self.last_input = input_value
        input_len, nodes = self.policy_weights.shape
        totals = np.dot(input_value, self.policy_weights) + self.biases
        self.last_totals = totals
        print("softmax linear result")
        print(totals)
        print("softmax activation result")
        print(self.softmax_activation_equation(totals))
        return self.softmax_activation_equation(totals)  
    
    def backward_prop(self, dl_dout, learn_rate):
        return None

    
    def softmax_activation_equation(self, totals):
        exp = np.exp(totals)
#        print('exp totals')
#        print(exp)
#        print(exp.ndim)
        if (exp.ndim == 1):
            return exp/np.sum(exp, axis=0)
        else:
            return exp/ np.sum(exp, axis=1, keepdims=True)
    
    def softmax_derivative_equation(self, totals):
        return None
        
    def set_target_weights(self, new_weights):
        self.target_weights = new_weights
    
    def get_target_weights(self):
        return self.target_weights
    
    def get_policy_weights(self):
        return self.policy_weights
    
    def get_biases(self):
        return self.biases
    
    def print_target_weights(self):
        print("Target Weights: \n")
        print(self.target_weights)
    
    def print_policy_weights(self):
        print("Policy Weights: \n")
        print(self.policy_weights)
        
    def print_biases(self):
        print(self.biases)