# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 17:56:28 2020

@author: Dingie
"""

import numpy as np

class Relu_Activation_H2:
    
    def __init__ (self, input_len, nodes):
        self.target_weights = np.random.randn(input_len, nodes)/ input_len
        self.policy_weights = self.target_weights
        self.biases = np.zeros(nodes)
    
    def forward_prop_action(self, input_value):
        input_value = input_value.flatten()
        totals = np.dot(input_value, self.policy_weights) + self.biases
        return self.relu_activation_equation(totals)
    
    def forward_prop_target(self, input_value):
#        print("inside forward_prop")
#        print(input_value)
        self.last_input_shape = input_value.shape
#        print("input value shape")
#        print(input_value.shape)
        self.last_input_value = input_value
        input_len, nodes = self.target_weights.shape
        totals = np.dot(input_value, self.target_weights) + self.biases
#        print("relu linear calculation result")
#        print(totals)
        self.last_totals = totals
#        print("relu activation calculation result")
#        print(self.relu_activation_equation(totals))
        return self.relu_activation_equation(totals)   
    
    def forward_prop(self, input_value):
#        print("inside forward_prop")
#        print(input_value)
        self.last_input_shape = input_value.shape
#        print("input value shape")
#        print(input_value.shape)
        self.last_input_value = input_value
        input_len, nodes = self.policy_weights.shape
        totals = np.dot(input_value, self.policy_weights) + self.biases
#        print("relu linear calculation result")
#        print(totals)
        self.last_totals = totals
#        print("relu activation calculation result")
#        print(self.relu_activation_equation(totals))
        return self.relu_activation_equation(totals)
    
    def backward_prop(self, d_net_d_out, d_error_d_net, learn_rate= 0.005):
        
#        print("Hidden layer backpropagation")        
#        print(d_net_d_out)
#        print(d_net_d_out.shape)
#        print(d_error_d_net)
#        print(d_error_d_net.shape)
        
        d_out_d_net = self.relu_derivative_equation(self.last_totals)
#        print(d_out_d_net)
#        print(d_out_d_net.shape)
#        
#        
#        print(self.last_input_value)
#        print(self.last_input_value.shape)
#        
#        print(self.policy_weights)
#        print(self.policy_weights.shape)
        
        d_error_d_out = np.dot(d_error_d_net, np.transpose(d_net_d_out))
        
        d_error_d_new_net= d_error_d_out * d_out_d_net
        
        d_error_d_w = np.dot(np.transpose(d_error_d_new_net), self.last_input_value)
        
        self.policy_weights = self.policy_weights - learn_rate * d_error_d_w
        
        return None
    
    def set_target_weights(self, new_weights):
        self.target_weights = new_weights
    
    def get_target_weights(self):
        return self.target_weights
    
    def get_policy_weights(self):
        return self.policy_weights
    
    def get_biases(self):
        return self.biases
    
    def relu_activation_equation(self, totals):
        totals = np.where(totals < 0, 0, totals)
        return totals
    
    def relu_derivative_equation(self, totals):
        totals = np.where(totals < 0, 0, totals)
        totals = np.where(totals > 0, 1, totals)
        return totals
    
    def save_weights(self):
        return None
            
    def print_target_weights(self):
        print("Target Weights: \n")
        print(self.target_weights)
        
    def print_policy_weights(self):
        print("Policy Weights: \n")
        print(self.policy_weights)

    def print_biases(self):
        print(self.biases)