# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 17:56:28 2020

@author: Dingie
"""

import numpy as np

class Tanh_Activation_H2:
    
    def __init__ (self, input_len, nodes):
        self.target_weights = np.random.randn(input_len, nodes)/ input_len
        self.policy_weights = np.random.randn(input_len, nodes)/ input_len
        self.biases = np.zeros(nodes)
    
    def forward_prop_action(self, input_value):
        input_value = input_value.flatten()
        totals = np.dot(input_value, self.policy_weights) + self.biases
        return self.tanh(totals)
    
    def forward_prop_target(self, input_value):
#        print("inside forward_prop")
#        print(input_value)
        self.last_input_shape = input_value.shape
#        print("input value shape")
#        print(input_value.shape)
        self.last_input_value = input_value
        input_len, nodes = self.target_weights.shape
        totals = np.dot(input_value, self.target_weights) + self.biases
#        print("relu linear calculation result")
#        print(totals)
        self.last_totals = totals
#        print("relu activation calculation result")
#        print(self.relu_activation_equation(totals))
        return self.tanh(totals)   
    
    def forward_prop(self, input_value):
#        print("inside forward_prop")
#        print(input_value)
        self.last_input_shape = input_value.shape
#        print("input value shape")
#        print(input_value.shape)
        self.last_input_value = input_value
        input_len, nodes = self.policy_weights.shape
        totals = np.dot(input_value, self.policy_weights) + self.biases
#        print("relu linear calculation result")
#        print(totals)
        self.last_totals = totals
#        print("relu activation calculation result")
#        print(self.relu_activation_equation(totals))
        return self.tanh(totals)
    
    def backward_prop(self, d_net_d_out, d_error_d_net, learn_rate= 0.00001 ):
        
#        print("Hidden layer backpropagation")        
#        print(d_net_d_out)
#        print(d_net_d_out.shape)
#        print(d_error_d_net)
#        print(d_error_d_net.shape)
        
        d_out_d_net = self.tanh_derivative(self.last_totals)
#        print(d_out_d_net)
#        print(d_out_d_net.shape)
#        
#        
#        print(self.last_input_value)
#        print(self.last_input_value.shape)
#        
#        print(self.policy_weights)
#        print(self.policy_weights.shape)
        
        d_error_d_out = np.dot( d_error_d_net, np.transpose(d_net_d_out))
        
        d_error_d_new_net= d_error_d_out * d_out_d_net
        
        d_net_d_w = np.transpose(self.last_input_value)
        d_error_d_w = np.dot(d_net_d_w, d_error_d_new_net)
        d_net_d_out_h2= self.policy_weights
        self.policy_weights = self.policy_weights - learn_rate * d_error_d_w
        
        return d_net_d_out_h2, d_out_d_net
    
    def set_target_weights(self, new_weights):
        self.target_weights = new_weights
    
    def get_target_weights(self):
        return self.target_weights
    
    def get_policy_weights(self):
        return self.policy_weights
    
    def get_biases(self):
        return self.biases
    
    def tanh(self, linearRegression):
        return (2/(1+np.exp(-2* linearRegression)))-1

    def tanh_derivative(self, tanh_output):
        return 1 - (self.tanh(tanh_output) ** 2)
    
    def save_weights(self):
        return None
            
    def print_target_weights(self):
        print("Target Weights: \n")
        print(self.target_weights)
        
    def print_policy_weights(self):
        print("Policy Weights: \n")
        print(self.policy_weights)

    def print_biases(self):
        print(self.biases)