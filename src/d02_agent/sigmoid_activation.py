# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 17:55:05 2020

@author: Dingie
"""

import numpy as np

class Sigmoid_Activation:
    
    def __init__(self, input_len, nodes):
        self.weights = np.random.randn(input_len, nodes)/ input_len
        self.biases = np.zeros(nodes)
    
    def forward_prop(self, input):
        return None
    
    def backward_prop(self, dl_dout, learn_rate):
        return None
    
    def get_weights(self):
        return self.weights
    
    def get_biases(self):
        return self.biases
    
    def print_weights(self):
        print(self.weights)
        
    def print_biases(self):
        print(self.biases)