# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 19:03:18 2020

@author: Dingie
"""
import sys
sys.path.insert(1, './d00_utils/')
sys.path.insert(2, './d01_data/')
sys.path.insert(3, './d02_agent/')

from map_gen_v2 import Map_Gen_V2
from memory_data_v2 import Memory_Data_V2
from neural_network_v2 import Neural_Network_V2
from robot import Robot
from datetime import datetime

def main():
    print("Main Robot Function")

    game_map = Map_Gen_V2()
    game_map.configure()
    game_map.check_configure()
    game_map.create_map()
    game_map.configure_save_map()
    map_info_array = game_map.get_map_info()
    
    
    memory = Memory_Data_V2()
    memory.configure(map_info_array[0], map_info_array[1], map_info_array[2])
    memory.check_configure()
    
    neural_network = Neural_Network_V2()
    neural_network.configure(map_info_array[0], map_info_array[1], map_info_array[2], "relu", "relu", "linear", 6)
    neural_network.generate_neural_network()
    neural_network.print_hidden_one_info()
    neural_network.print_hidden_two_info()
    neural_network.print_output_info()
    
    is_experiment_on_going = True
    
    robot = Robot()
    robot.configure_game_map(game_map)
    robot.configure_memory_data(memory)
    robot.configure_neural_network(neural_network)
    robot.configure_robot_simulation()
    
    
    while (is_experiment_on_going):
        robot.increment_iter()
        action_selected = robot.select_explore_exploit()
        move_selected = robot.execute_action(action_selected)
        robot.configure_new_state(move_selected)
        robot.store_experience()


        is_experience_capacity_reached = robot.check_store_experience_len()
        if (is_experience_capacity_reached):
            # starts the real weight update process here
            # increment backpropagation process
            robot.increment_weight_update_counter()
            if (robot.check_weight_update_counter()):
                robot.implement_weight_update()
            robot.increment_decision_selection_counter()
            if (robot.check_decision_selection_counter()):
                robot.implement_decision_selection()
            robot.update_weights()
            robot.print_iter_result()
            
        is_full = robot.check_store_experience_capacity()
        
        if (is_full):
            robot.remove_first_experience()
        is_experiment_on_going = robot.is_experiment_on_going()
        
        
#        print("Move selected: " + str(move_selected))
#        robot.print_experience_replay_array()
#        robot.print_error()
#        exit_early = input("Exit early: (q)")
#        if (exit_early == "q"):
#            is_experiment_on_going = False
    current_time = datetime.now()
    neural_network.save_hidden_layer_one(current_time.strftime("%M%H%d%m%Y"))
    neural_network.save_hidden_layer_two(current_time.strftime("%M%H%d%m%Y"))
    neural_network.save_output_layer(current_time.strftime("%M%H%d%m%Y"))
    
    
    

if __name__ == "__main__":
    main()