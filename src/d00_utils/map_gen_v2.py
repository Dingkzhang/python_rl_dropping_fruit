# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 19:05:40 2020

@author: Dingie
"""

from os import path
import sys
import numpy as np
import pandas as pd
class Map_Gen_V2:
    
    INIT_PATH = "../data/01_utils/maps/"
    
    correct_configure = None
    map_type_name = ""
    map_file_name = ""
    
    map_width = None
    map_height = None
    map_vision = None
    
    default_width = 5
    min_width = 3
    max_width = 7
    
    default_height = 30
    min_height = 10
    max_height = 500500
    
    default_vision = 5
    min_vision = 3
    max_vision = 7
    
    min_reward = -5
    max_reward = 5   
    game_map = None
    
    def __init__(self):
        print("Initializing Map_Gen_V2")
        return None
    
    def configure(self):
        self.map_type_name = input("Input Map Type (random/existing): ")    
        self.map_file_name = input("Input map file name. Skip if selected random. (add .csv to end): ")        
        self.map_width = input("Input map width. Skip if selected existing: ")
        self.map_height = input("Input map height. Skip if selected existing: ")
        self.map_vision = input("Input map vision: ")

    def check_configure(self):
        
        map_type_check_result = self.check_map_type_name()
        
        if (map_type_check_result == 'r'):
            print("Skipping map name check. Reason: Random map generation selected")
        elif(map_type_check_result == 'e'):
            print("Existing map type selected. Issuing file name check")    
            if(self.check_map_file_name() == False):
                sys.exit()
        else:
            sys.exit()
            
        self.check_map_param()
        print("Map check completed.")
        return None
    
    def check_map_type_name(self):
        
        if (self.map_type_name.lower() == "random"):
            print("Map type set to random confirmed")
            return 'r'    
        elif (self.map_type_name.lower() == "existing"):
            print("Map type set to existing confirmed")
            return 'e'
        print("ERROR: Map type not set to correct value. Exiting out of system")
        return None
    
    def check_map_file_name(self):
        if(self.map_file_name != "" and path.exists(self.INIT_PATH + self.map_file_name)):
            print("Map name found")
            return True
        print("ERROR: Map name not found. Exiting out of system.")
        return False
    
    # if random then check for input value # if existing then fetch existing map value as values
    def check_map_param(self):
        if (self.map_type_name.lower() == "random"):
            self.check_random_input()
        self.check_vision_input()
    
    def check_vision_input(self):
        if(int(self.map_vision) > self.max_vision or int(self.map_vision) < self.min_vision):
            print("map_vision not configured correctly, switching to default map_vision of 5")
            self.map_vision = self.default_vision
        else:
            print("map_vision configured correctly, map_vision set to: " + str(self.map_vision))
            self.map_vision = int(self.map_vision)
    
    def check_random_input(self):
        if(int(self.map_width) > self.max_width or int(self.map_width) < self.min_width):
            print("map_width not configured correctly, switching to default map_width of 5")
            self.map_width = self.default_width
        else:
            print("map_width configured correctly, map_width set to: " + str(self.map_width))
            self.map_width = int(self.map_width)
        
        if(int(self.map_height) > self.max_height or int(self.map_height) < self.min_height):
            print("map_width not configured correctly, switching to default map_height of 10")
            self.map_height = self.default_height
        else:
            print("map_height configured correctly, map_height set to: " + str(self.map_height))
            self.map_height = int(self.map_height)

    def create_map(self):
        if (self.map_type_name.lower() == "random"):
            self.create_random_map()
        elif(self.map_type_name.lower() == "existing"):
            self.create_existing_map()
        self.print_map()
        self.print_map_info()

    def create_random_map(self):
        print("Generating random map.")
        self.game_map = np.random.uniform(self.min_reward, self.max_reward, (self.map_height, self.map_width))
        self.game_map = self.game_map.round(decimals=2)
    
    def create_existing_map(self):
        print("Retrieving existing map.")
        map_data = pd.read_csv(self.INIT_PATH + self.map_file_name, header=None)
        self.game_map = map_data.to_numpy()
        self.map_height = map_data.to_numpy().shape[0]
        self.map_width = map_data.to_numpy().shape[1]
        
    def configure_save_map(self):
        save_map_choice = input("Does the user want to save the map? (y/n)")
        if (save_map_choice == "y"):
            print("User expresses interest in saving map.")
            map_name = input("Please enter 3-12 character alphanumeric characters for map name: ")
            self.check_map_name(map_name)
        else:
            print("User does not want to save map. Proceeding with rest of program.")
        return None
    
    def check_map_name(self, map_name):
        if (len(map_name) < 3 or len(map_name) > 12 or not map_name.isalnum()):
            print("Invalid map name. Map will not be saved.")
        else:
            print("Valid map name. Checking to see if map name is available.")
            if (path.exists(self.INIT_PATH + map_name + ".csv")):
                print("Map name exist. Map will not be saved.")
            else:
                print("Map name does not exist. Saving map...")
                self.save_map(map_name)

    def save_map(self, map_name):
        np.savetxt(self.INIT_PATH + map_name + ".csv", self.game_map, delimiter=",")
        print("Map name saved successfully.")
        
    def get_map_info(self):
        return [self.map_width, self.map_height, self.map_vision]
    
    def get_map(self):
        return self.game_map
    
    ###################### PRINT STATEMENTS ######################

    def print_map_info(self):
        
        format_string = "{:<20s}{:>20s}"    
        print(format_string.format("Map Width: ", str(self.map_width)))
        print(format_string.format("Map Height: ", str(self.map_height)))
        print(format_string.format("Map Vision: ", str(self.map_vision)))
    
    def print_map(self):
        print("This is the generated map.")
        print(self.game_map,"\n")
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    